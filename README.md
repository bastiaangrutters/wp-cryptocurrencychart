# WordPress plugin for CryptoCurrencyChart

WordPress plugin to show cryptocurrency price data in Widgets from the cryptocurrencychart.com API. Select any of the 1,000+ available crypto currencies to show the latest price in widgets around your site.

Requires a [free CryptoCurrencyChart subscription](https://www.cryptocurrencychart.com/api/documentation)